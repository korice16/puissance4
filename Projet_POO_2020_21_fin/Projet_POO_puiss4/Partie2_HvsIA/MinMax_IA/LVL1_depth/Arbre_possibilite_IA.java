package Partie2_HvsIA.MinMax_IA.LVL1_depth;

import Partie1_HvsH.*;
import Partie1_HvsH.ClasseException.*;
import Partie2_HvsIA.HvsIA_naive.Arbre.*;

/**
 * D�crivez votre classe Arbre_possibilite_IA ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class Arbre_possibilite_IA
{
    // variables d'instance - remplacez l'exemple qui suit par le v�tre
    private int x;

    /**
     * Constructeur d'objets de classe Arbre_possibilite_IA
     */
    public Arbre_possibilite_IA(){
        // initialisation des variables d'instance
        
    }

    public static char[][] play_column1(char[][] tray){
        char[][] grille2=Tray.copie(tray);
        Tree tree=new Tree(tray,5);
        Tree.score_tree(tree,5);
        char[][] grille1=renvoie(tree);
        /*
        int z=0;
        tree.display_tree_possibilities(tree);
            for(int y=0;y<tray[0].length;y++){
                char[][] grille=Tray.copie(grille2);
                try{
                    int line=Tray.play_column(grille,'O',y);
                    if(grille[line-1][y]!=grille1[line-1][y]) z=y;
                }catch (ColumnException e){
        
                } 
                
        
            }
        */
        
        return grille1;
    }
    
    public static int play_column(char[][] tray){
        char[][] grille2=Tray.copie(tray);
        Tree tree=new Tree(tray,5);
        Tree.score_tree(tree,5);
        char[][] grille1=renvoie(tree);
        int z=0;
        tree.display_tree_possibilities(tree);
            for(int y=0;y<tray[0].length;y++){
                char[][] grille=Tray.copie(grille2);
                try{
                    int line=Tray.play_column(grille,'O',y);
                    if(grille[line-1][y]!=grille1[line-1][y]) z=y;
                }catch (ColumnException e){
        
                } 
                
        
            }
        
        
        return z;
    }
    
    public static char[][] renvoie(Tree tree){
        char[][] grille=tree.getRoot().getChildren().getFirst().getNoeud().getTray();
        int score=tree.getRoot().getChildren().getFirst().getNoeud().getScore();
        if(tree.getRoot().getChildren()!=null){
            Maillon cour=tree.getRoot().getChildren().getFirst();
            while(cour!=null){
                /*char[][] grille=cour.getNoeud().getTray();
                display_tray(grille,espace);*/
                if(score<=cour.getNoeud().getScore()) {
                    score=cour.getNoeud().getScore();
                    grille=cour.getNoeud().getTray();
                }
                cour=cour.getNext_brother();
            } 
        }
        return grille;
    }
}
