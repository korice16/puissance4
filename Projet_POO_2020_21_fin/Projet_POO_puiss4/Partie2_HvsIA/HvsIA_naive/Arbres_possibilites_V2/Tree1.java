package Partie2_HvsIA.HvsIA_naive.Arbres_possibilites_V2;
 
public class Tree1
{
    // variables d'instance - remplacez l'exemple qui suit par le v�tre
    private Node racine;
    public Tree1()
    {
        // initialisation des variables d'instance
        racine = null;
    }   
   
    public Tree1(Node racine)
    {
        // initialisation des variables d'instance
        this.racine = racine;
    }   
  
    /**
     * creat_Tree1
     * @param l la ligne de la derniere piece jouer 
     * @param c la colonne de la derniere piece jouer
     * @param o la char pour alterner 'x' ou 'o'
     * @return renvoie Tree1
     */
    public static Tree1 creat_Tree1(int c, int l, char  o)
    { 
        if(c<=0 || l<=0) return null;
        Tree1 t=new Tree1(new Node(Grille1.grille_vide(l,c)));//initialiser le racine par grille vide 
        creat_Tree1_aux(o,t.racine,2*c);
        return t;
    }
   
    /**
     * creat_Tree1_aux
     * @param t la Node 
     * @param p la profondre d'arbre t
     * @return renvoie List qui consisit les different posibilite
     */
    //creat_Tree1_aux est une method pour create une list d'appre la node t 
    private static List creat_Tree1_aux(char c, Node t,int p){
        if(p<0)return null;
        if(t==null)return null;  
        if(Gagner1.verifier_Gagner1_total(t.getInfo())){
            t.setChildren(null);
            return t.getChildren();
        } else{ 
            List l = List.creetList(t.getInfo(),c);  
            t.setChildren(l);          
            creat_list (c,t.getChildren(),p-1);   
            return l;     
        }
    }
    /**
     * creat_list
     * @param l la List
     * @param p la profondre d'arbre t
     * @param c la char pour alterner 'x' ou 'o'
     * il apple une method creat_list_aux
     * @return rien
     */
     // creat_list est une method pour create une list pour tous le Maillon
    private static void creat_list(char c,List l,int p){ 
         if(p<0)return ;
         if(l==null)return;        
         Maillon current=l.getPremier();
         creat_list_aux(c,current,p);//,p  
        }
   
    /**
     * creat_list_aux
     * @param m est Maillon
     * @param p la profondre d'arbre t
     * @param c la char pour alterner 'x' ou 'o'
     * @return renvoie List qui consisit les different posibilite
     */     
    // creat_list_aux est une method pour create une list pour tous le Maillon
    private static List creat_list_aux(char c,Maillon m ,int p){
        if(p<0)return null;            
        if(m!=null)
        {      
            if(Gagner1.verifier_Gagner1_total(m.getN().getInfo())){   
                m.getN().setChildren(null);
                creat_list_aux(c,m.getNext(),p);
            }        
            List k=List.creetList(m.getN().getInfo(),c); 
            m.getN().setChildren(k); 
            deuxime_apple(c,k,p-1);                         
            creat_list_aux(c,m.getNext(),p);           
            return m.getN().getChildren(); 
            
        }  
         return null;
    } 
   /**
     * deuxime_apple
     * @param l est list
     * @param p la profondre d'arbre t
     *  @param c la char pour alterner 'x' ou 'o'
     * @return rien
     */
    //deuxime_apple est une method pour create une list pour tous le Maillon dans une list par appler une method deuxime_apple_aux
    private static void deuxime_apple(char c,List l,int p){
        if(l==null)return;
        Maillon current=l.getPremier();
        deuxime_apple_aux(c,current,p);
        
    }
   /**
     * deuxime_apple_aux
     * @param m Maillon
     * @param p la profondre d'arbre t
     * @param c la char pour alterner 'x' ou 'o'
     * @return renvoie rien
     */
    private static void deuxime_apple_aux(char c,Maillon m,int p){  
        if(m!=null){
            creat_Tree1_aux(c,m.getN(),p);
            deuxime_apple_aux(c,m.getNext(),p);   
        }    
              
    }
   
    public static void display_tree_possibilities(Tree1 tree){
        display_tree_possibilities_aux(tree.racine,2,0);
       
    }
    
    private static void display_tree_possibilities_aux(Node n,int prof,int espace){
        if(n==null || prof<0) return ;
        
        display_tray(n.getInfo(),espace);

        if(n.getChildren()!=null){
            Maillon cour=n.getChildren().getPremier();
            while(cour!=null){
                
                display_tree_possibilities_aux(cour.getN(), prof-1,espace+1);
                cour=cour.getNext();
            } 
        }
    }

    
    /**
     * methode pour afficher le contenu de la grille avec un certains charadisign
     * @param plateau le plateau du jeu
     */
    private static void display_tray(char[][] plateau,int espace){
        for(int x=0;x<espace;x++) System.out.print("      ");
        System.out.println();
        for(int l=0;l<plateau.length;l++){
            
            if(l==0){//avant la cretaion de la grille affiche les colonnes jouales
               
                
                for(int x=0;x<espace;x++) System.out.print("      ");
                
                for(int l1=0;l1<=(plateau[0].length)*4;l1++){//des trait pour separer les numeros des colonnes jouables et la grille
                    if(l1==0)System.out.print("");
                    System.out.print("=");
                }
                System.out.println();
            }
            
            for(int x=0;x<espace;x++) System.out.print("      ");
            
            for(int l2=0;l2<plateau[l].length;l2++){
               
                System.out.print("| "+plateau[l][l2]+" ");
            }
            
            System.out.println("|");
            
            for(int l1=0;l1<=(plateau[0].length)*4;l1++){
                    if(l1==0){
                        for(int x=0;x<espace;x++) System.out.print("      ");
                        System.out.print("");
                    }
                    System.out.print("=");
                }
            System.out.println();
            
        }
    }
    
    
}

   
