package Partie2_HvsIA.HvsIA_naive.Arbres_possibilites_V2;

public class List
{
    private Maillon first;

    public List()
    {
        first = null;
    }

    public List(Maillon m)
    {
        first = m;
    }

    public boolean isListVide() {
        return (first == null);
    }

    public Maillon getPremier()
    {
        return first;
    }
   
    /**
     * creetList
     * @param t la grille du jeu
     * @param u la char pour alterner 'x' ou 'o'
     * 
     * @return renvoie List si t n'est pas null ou plein
     */
   public static List creetList( char[][] t, char u) {
           if(t==null) return null;
           if(Grille1.grille_plein(t))return null;     
           //if(Gagner.gagner_partie(t,t.length-1,column-1))return null;
           int column=t[0].length;
           List l = new List();        
           char[][] t1= Grille1.grille_copie(t);   
           Maillon current=null;
           int j=1;
           while(Grille1.is_column_full(t1 ,j)){
               if(j<t[0].length)j++;       
              else break;
            }
            current = l.first= new Maillon(new Node(Grille1.AjouteElement_index(Grille1.grille_copie(t1),Grille1.x_ou_o(t1),j)));      
            int i=j+1 ;
           while (i <= column)
            {
                
                current.setNext(new Maillon(new Node (Grille1.AjouteElement_index(Grille1.grille_copie(t1),Grille1.x_ou_o(t1),i))));
                current=current.getNext(); 
                  i++;
             }
           return l; 
        }
}
    

  