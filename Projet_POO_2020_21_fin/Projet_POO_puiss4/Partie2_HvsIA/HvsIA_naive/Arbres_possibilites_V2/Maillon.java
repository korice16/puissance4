package Partie2_HvsIA.HvsIA_naive.Arbres_possibilites_V2;

public class Maillon
{
    // variables d'instance - remplacez l'exemple qui suit par le v�tre
    private Node n;
    private Maillon next ;
    /**
     * Constructeur d'objets de classe Maillon
     */
    public Maillon()
    {
        // initialisation des variables d'instance
        this.n=null;
        this.next=null;
    } public Maillon(Node n)
    {
        // initialisation des variables d'instance
        this.n=n;
        this.next=null;
    }
    
     public Node getN()
    {
        return n ;
    }
    public void setN(Node n)
    {
         this.n=n ;
    }
    public void setNext(Maillon m)
    {
         this.next=m;
    }
    public Maillon getNext()
    {
         return next;
    }
}
