package Partie2_HvsIA.HvsIA_naive.Arbres_possibilites_V2;

 class Node 
{
    private char[][] info;
    private List children;
    public Node(char[][] val)
    {
        this.info = val;       
        this.children=null;
        //this.precedent = null;
    }

    public Node(char[][] val, Node f)//Node s
    {
        this.info = val;
      
        this.children=null;
    }
 
    public List getChildren()
    {
        return children;
    }
    
    public void setChildren(List suiv){
        children= suiv;
    } 
    
    public char[][] getInfo()
    {
        return info;
    }
   
    public void setInfo(char[][] h)
    {
         info=h;
    }
    
    @Override
    public String toString()
    {
        return "->"+ this.info +this.children ;
    }

}
