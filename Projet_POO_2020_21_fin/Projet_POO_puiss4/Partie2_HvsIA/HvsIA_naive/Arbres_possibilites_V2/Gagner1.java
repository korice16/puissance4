 package Partie2_HvsIA.HvsIA_naive.Arbres_possibilites_V2;
   
/**
 * D�crivez votre classe Gagner1 ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class Gagner1
{
    // variables d'instance - remplacez l'exemple qui suit par le v�tre
    
    /**
     * Constructeur d'objets de classe Gagner1
     */
    public Gagner1()
    {
        // initialisation des variables d'instance
       
    }

    /**
     * possibilit� de Gagner1 de facon diagonale diagonale vers le bas droite (i<j)
     * @param grille la grille du jeu
     * @return renvoie Vrai si quatre piece consecutif de la meme couleur en diagonale sont align�e  sinon Faux
     */
    
       public static boolean verifier_Gagner1_diagonalA(char[][] grille){
           if(grille!=null){
                int m=0; int y=0;  int a=1;    int b=0;    
                for(int i=1; i<grille.length-1;i++){   
                    a=i; m=0; y=0; b=0; 
                    while(a<grille.length||b<grille[0].length){
                        if(grille[a][b]=='x')m++;    else m=0;
                       
                        if(grille[a][b]=='o') y++; else y=0;
                        
                        if(m==2||y==2)return true;    
                        a++;
                        b++;                
                    }
                }
                return false;   
            }
            return false;    
        }
    
    /**
     * possibilit� de Gagner1 de facon diagonale vers le bas droite(i>j)
     * @param tray la grille du jeu
     * 
     * @return renvoie Vrai si quatre piece consecutif de la meme couleur en diagonale sont align�e  sinon Faux
     */
  
     public static boolean verifier_Gagner1_diagonalB(char[][] grille){
         if(grille!=null){
             int m=0; int y=0;  int a=0;    int b=0;    
             for(int j=1; j<grille[0].length;j++){  
                 b=j; m=0; y=0;//a=0;
                  for(int i=1; i<grille.length-1;i++){  
                                 
                    if(grille[i][j]=='x')m++;  else m=0;
                     
                     if(grille[i][j]=='o') y++;  else y=0;
                   
                     if(m==4||y==4)return true;    
                    
                                     
                    }
                }
                return false;
            }
            return false;
        }
    
    
    /**
     * possibilit� de Gagner1 de facon verticale
     * @param grille la grille du jeu
     * @return renvoie Vrai si quatre piece consecutif de la meme couleur en verticale sont align�e  sinon Faux
     */
    public static boolean verifier_Gagner1_vertical(char[][] grille){
        if(grille!=null){
            int x=0;int y=0;           
            for(int j=0; j<grille[0].length;j++){
                x=0;  y=0;
                for(int i=grille.length-1; i>=0;i--){
                  if(x==4||y==4)return true;
                  if(grille[i][j]=='x')x++;
                   else x=0;
                  if(grille[i][j]=='o')y++;  
                   else y=0;
                }                
            } 
            return false;
        }   
        return false;
    }
    
    /**
     * possibilit� de Gagner1 de facon horizontale
     * @param tray la grille du jeu
     * 
     * @return renvoie Vrai si quatre piece consecutif de la meme couleur en horizontal sont align�e  sinon Faux
     */
    public static boolean verifier_Gagner1_horizontal(char[][] grille){
        if(grille!=null){
            int x=0;int y=0;
            for(int i=grille.length-1;i>=0;i--){  
                x=0;  y=0;
                for(int j=0;j<grille[0].length; j++){
                  if(x==4||y==4)return true;
                  if(grille[i][j]=='x')x++;
                  else x=0;
                  if(grille[i][j]=='o')y++;
                }                            
            }    
              return false;
        }   
        return false;
    }
    
    /**
     * possibilit� de Gagner1 de facon diagonal (i==j)
     * verifier_Gagner1_diagonalc
     * @param grille la grille du jeu
     * 
     * @return renvoie Vrai si la partie � �t� gagn�e sinon Faux
     */
    public static boolean verifier_Gagner1_diagonalc(char[][] grille){
        if(grille!=null){
            int m=0; int y=0;  int a=0;    int b=0;    
            while(a<grille.length||b<grille[0].length){                     
                        if(grille[a][b]=='x')m++;  
                        else m=0;
                        if(grille[a][b]=='o') y++;
                        else y=0;
                        if(m==4||y==4)return true;    
                        a++;
                        b++;                
                    }
        }
          return false;
    }
    /**
     * possibilit� de Gagner1 
     * verifier_Gagner1_total
     * @param grille la grille du jeu
     * 
     * @return renvoie Vrai si la partie � �t� gagn�e sinon Faux
     */

 
    public static boolean verifier_Gagner1_total(char[][] grille){
        if(grille!=null){
            if(verifier_Gagner1_horizontal(grille)||verifier_Gagner1_vertical(grille)
            ||verifier_Gagner1_diagonalc(grille)||verifier_Gagner1_diagonalB(grille)||verifier_Gagner1_diagonalB(grille)) return true;
            
        }
        return false;   
    }


}
