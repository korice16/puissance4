package Partie2_HvsIA.HvsIA_naive.Arbres_possibilites_V2;
import java.util.Scanner;
import java.io.*; //FileWriter;
import java.io.File;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.FileNotFoundException; 
import java.util.NoSuchElementException;
import java.nio.file.Path;
import java.nio.file.Paths;
import Partie1_HvsH.*;
public class Grille1
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private char[][] grille;
  
    /**
     * Constructeur d'objets de classe Grille
     */
    public Grille1(){   
    } 
    
    public Grille1(int l_taille,int c_taille)
    {
        // initialisation des variables d'instance
        this.grille= new char[l_taille][c_taille];
    }  
     /**
     * grille_vide
     * @param l_taille la ligne  
     * @param c_taillela colonne  
     * @return renvoie grille du jeu
     */
     
    public static char[][] grille_vide(int l_taille,int c_taille)
    {
        // initialisation des variables d'instance
        char[][]grille= new char[l_taille][c_taille];
        for(int i=0; i<grille.length; i++){
            for(int j=0; j<grille[i].length;j++){
                    grille[i][j]=EnumCase.getEnumCase(EnumCase.VIDE);
                }
            }
            return grille;
      }  
    /**
     * grille_vide
     * @param grille la grille du jeu
     * @return renvoie vrais si la girlle est plein  
     */ 
    
    public boolean grille_vide(){
            for(int i=0; i<grille.length; i++){
                for(int j=0; j<grille[i].length;j++){
                if( grille[i][j] != EnumCase.getEnumCase(EnumCase.VIDE))
                return false;
            }
        }
        return true;
    }
    /**
     * grille_plein
     * @param grille la grille du jeu
     * @return renvoie vrais si la girlle est plein  
     */ 
     public static boolean grille_plein(char[][] grille){
        //if(grille==null)return true ;
        for(int i=0; i<grille.length; i++){
            for(int j=0; j<grille[i].length;j++){
                if(grille[i][j]== EnumCase.getEnumCase(EnumCase.VIDE));//EnumCase.getEnumCase(EnumCase.VIDE)) 
                return false ;
            }
        }
       return true;
      }  
    /**
     * AjouteElement_index
     * @param grille1 la grille du jeu
     * @param p la char pour alterner 'x' ou 'o'
     * @param nuColumn la Column ou on va jouer 
     * @return renvoie la grille si grille1 n'est pas null ou plein
     */ 
     public static char[][] AjouteElement_index(char[][] grille1,char p,int nuColumn){     
        int column1=nuColumn-1;
        if(!is_column_full(grille1,nuColumn)){
            for(int j=grille1.length-1; j>-1; j--){
                if(grille1[j][column1]==EnumCase.getEnumCase(EnumCase.VIDE)){
                    grille1[j][column1]=p;                    
                    break;
                }       
            }
              column1++;
              return grille1;
        }
        return null;
    }
    /**
     * is_column_full
     * @param grille1 la grille du jeu
     *  @param nuColumn la Column ou on va jouer 
     * @return renvoie true  si column est  plein si non renvoie false
     */
    public static boolean is_column_full(char[][] grille1,int column){//
        if(column>grille1[0].length) return true;
         int column1=column-1;    
              for(int j=0; j<grille1.length;j++){
                if(grille1[j][column1]==EnumCase.getEnumCase(EnumCase.VIDE))
                    return false ;
            }
            return true;  
        } 
    /**
     * grille_copie
     * @param t la grille du jeu
     * @return renvoie un nouvau la copie de t si t nest pas null
     */
    public static char[][] grille_copie(char[][] t){
        if(t!=null){
            char[][] newgrille=new char[t.length][t[0].length];
            for(int i=0;i<t.length;i++){
                for(int j=0;j<t[0].length;j++){
                    newgrille[i][j]=t[i][j];
                }
            }  
            return newgrille;
        }return null;
    }
    //pour alterner le joures 
     /**
     * x_ou_o
     * @param t la grille du jeu  
     * @return renvoie un char x ou o 
     */
    public static char x_ou_o(char[][] t)
    {
        int x=0;
        int o=0;
       for(int i=0;i<t.length;i++){
         for(int j=0;j<t[0].length;j++){
             if(t[i][j]=='x')x++;
             if(t[i][j]=='o')o++;
          }
        }   
        if(x>o)return 'o';
        else if(x==o)return 'x';
        else return 'x';
       
    }  
   
    }