package Partie2_HvsIA.HvsIA_naive.Arbre;
import Partie1_HvsH.*;
import Partie1_HvsH.ClasseException.*;

/**
 * D�crivez votre classe Tree ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class Tree
{
    
    private Node root;
    private static int prof_max;
    //private char[][] grille;
    /**
     * Constructeur d'objets de classe Tree
     * @param grille grille de base du jeu
     * @param prof la profonedeur max de l'arbre
     */
    public Tree(char[][] grille,int prof)
    {
        // initialisation des variables d'instance
        this.root=new Node(grille);
        root.jeton_couleur();
        
        if(prof>0) root.setChildren( new List (List.cree_tree_possibilities_aux1(Tray.copie(grille),root.getTour(),0,prof ) ));
        this.prof_max=prof;
    }
    
    /**
     * Constructeur surcharger
     * @param l le nombre de ligne pour construire un tableau � 2D
     * @param c le nombre de colonne pour construire un tableau � 2D
     * @param prof la profondeur max de l'arbre 
     */
    public Tree(int l,int c,int prof)
    {
        // initialisation des variables d'instance
        char[][] grille=Tray.build_grille_vide(l,c);
        this.root=new Node(grille);
        root.jeton_couleur();
        if(prof>0) root.setChildren( new List (List.cree_tree_possibilities_aux1(Tray.copie(grille),root.getTour(),0,prof ) )); 
        this.prof_max=prof;
    }
    
    public Node getRoot(){
        return this.root;
    }
    
    public static void score_tree(Tree tree,int prof){
        int b=score(tree.root,prof);
        tree.root.setScore(b);
    }
    
    private static int score(Node n,int prof){
        if(n==null || prof<0 || n.getChildren()==null) return n.getScore();
        
        if(prof==0  ){
            Maillon cour=n.getChildren().getFirst();
            int score_max=0;
            while(cour!=null && ( score_max!=1 || score_max!=-1) ){
                if( n.getTour()=='O'){ //tour de l'IA
                    //if(cour.getNoeud().getScore()==1) score_max=1;
                    //score_max=1;
                    
                     if (evaluation(cour.getNoeud().getTray())==-1) score_max=-1;
                    else if (evaluation(cour.getNoeud().getTray())==1) score_max=1;
                }
            
                if( n.getTour()=='X'){ //tour de l'IA
                    //if(cour.getNoeud().getScore()==-1) score_max=-1;
                    //score_max=-1;
                    
                     if (evaluation(cour.getNoeud().getTray())==1) score_max=1;
                    else if (evaluation(cour.getNoeud().getTray())==-1) score_max=-1;
                }
            
                cour=cour.getNext_brother();
            }
            return score_max;
        }
        /*
        if(n.getChildren()!=null){
            Maillon cour=n.getChildren().getFirst();
            while(cour!=null){

                display_tree_possibilities_aux(cour.getNoeud(), prof-1,espace+1);
                cour=cour.getNext_brother();
            } 
        }*/
        
        Maillon cour_brother=n.getChildren().getFirst();
        while(cour_brother!=null){
            n.setScore(score(cour_brother.getNoeud(), prof-1));
            cour_brother=cour_brother.getNext_brother();
        }
        Maillon cour=n.getChildren().getFirst();
        int score_max=cour.getNoeud().getScore();
        while(cour!=null && ( score_max!=1 || score_max!=-1) ){
            if(n.getTour()=='O'){
                if(score_max<=cour.getNoeud().getScore()) {
                    score_max=cour.getNoeud().getScore();
                    
                }
            }else {
                if(score_max<=cour.getNoeud().getScore()) {
                    score_max=cour.getNoeud().getScore();
                    
                }
            }
            
            cour=cour.getNext_brother();
        }
        //n.setScore(score(n,prof-1));
        return score_max;
    }
    
    public static void test(Tree tree){
        test_aux(tree.root,4);
        //Node cour=tree.root;
        /*
        System.out.println(cour.getScore()+" ");
        System.out.println(cour.getTour()+" ");
        /*
        while(cour!=null){
            System.out.println(cour.getScore()+" ");
            //cour=cour.getNext_brother();
        }*/
        /*
        Maillon cour1=tree.root.getChildren().getFirst();
        while(cour1!=null){
            char[][] grille=cour1.getNoeud().getTray();;
            Tray.display_tray(grille);
            cour1=cour1.getNext_brother();
        }*/
    }
    
    public static void test_aux(Node n,int prof){
        if(n==null || prof<0 || n.getChildren()==null || n.getScore()==1 || n.getScore()==-1) return ;//si n est null
        /*if(n.getChildren()==null){
        
        }*/
        else if(prof==0  ){//la prodendeur � �t� atteint et le neoud � une liste
                
                Maillon cour=n.getChildren().getFirst();
                int score_max=0;
                /*
                while(cour!=null && ( score_max!=1 || score_max!=-1)){
                    if(cour.getNoeud().getScore()==1 && n.getTour()=='O') score_max=1;
                    if(cour.getNoeud().getScore()==-1  && n.getTour()=='x') score_max=-1;
                    
                    cour=cour.getNext_brother();
                }*/
                n.setScore(score_max);
                return;
        }
        Maillon cour_brother=n.getChildren().getFirst();
        while(cour_brother!=null){
            test_aux(cour_brother.getNoeud(), prof-1);
            cour_brother=cour_brother.getNext_brother();
        }
        Maillon cour=n.getChildren().getFirst();
        int score_max=0;
        while(cour!=null && ( score_max!=1 || score_max!=-1) ){
            if( n.getTour()=='O'){ //tour de l'IA
               if(cour.getNoeud().getScore()==1) score_max=1;
               else if (evaluation(cour.getNoeud().getTray())==-1) score_max=-1;
               else if (evaluation(cour.getNoeud().getTray())==1) score_max=1;
            }
            
            if( n.getTour()=='X'){ //tour de l'IA
               if(cour.getNoeud().getScore()==-1) score_max=-1;
               else if (evaluation(cour.getNoeud().getTray())==1) score_max=1;
               else if (evaluation(cour.getNoeud().getTray())==-1) score_max=-1;
            }
            
            cour=cour.getNext_brother();
        }
        n.setScore(score_max);
        return ;
    }
     
    
    public static int evaluation(char[][] tray){
        
        //verifie si l'adversaire � une possiblede gagner danse ce cas ne pas aller dans cette feuille
        try{
            
            for(int x=0;x<tray[0].length;x++){
                char[][] grille=Tray.copie(tray);
                int line=Tray.play_column(grille,EnumCase.getEnumCase(EnumCase.X) ,x);//l'IA fait une simulation en jouant le jeton de l'adversaire
                if(Gagner.win_game(grille,line,x)) return -1;//verifie si l'adversaire a gagner apres avoir jouer son jeton,
                                                                //s'il gagne alors il renvoie la colonne o� il a du jouer pour gagner, pour empecher l'adversaire de gagner
            }
        }catch (ColumnException e){
        
        }
         
        //verifie si l'IA � plus de 3 jeton sont align�, si oui donc il a une possibilit� de victoire
        try{
            for(int x=0;x<tray[0].length;x++){
                char[][] grille=Tray.copie(tray);//ne pas alterer le veritable grille; et car on fait plusieurs test avec une nouvelle grille � chaque fois
                int line=Tray.play_column(grille,EnumCase.getEnumCase(EnumCase.O) ,x);//l'IA fait une simulation en jouant son jeton
                if(Gagner.win_game(grille,line,x)) return 1;//verifie s'il a gagner apres avoir jouer, s'il gagne alors il renvoie la colonne o� il a du jouer pour gagner
            }
        }catch (ColumnException e){
        
        } 
         
        return 0;//aucune victoire possible
    }
    
    /**
     * Fonction qui affiche l'arbre des possibilite jusqu'� une profendeur donn� (ATTENTION pas l'hauteur)
     * @see display_tree_possibilities_aux(Node,int, int)
     */
    public static void display_tree_possibilities(Tree tree){
        display_tree_possibilities_aux(tree.root,2,0);
        //display_tray(tree.root.getChildren().getFirst().getNoeud().getTray(),1);
        //display_tray(tree.root.getChildren().getFirst().getNext_brother().getNoeud().getTray(),1);
    }
    
    /**
     * Fonction auxiliaire pour afficher l'arbre des possibilite
     * @see display_tree_possibilities(Tree)
     */
    private static void display_tree_possibilities_aux(Node n,int prof,int espace){
        if(n==null || prof<0) return ;
        
        display_tray(n.getTray(),espace);

        if(n.getChildren()!=null){
            Maillon cour=n.getChildren().getFirst();
            while(cour!=null){
                /*char[][] grille=cour.getNoeud().getTray();
                display_tray(grille,espace);*/
                display_tree_possibilities_aux(cour.getNoeud(), prof-1,espace+1);
                cour=cour.getNext_brother();
            } 
        }
    }

    /**
     * Fonction auxiliaire pour afficher l'arbre des possibilite
     * @see display_tree_possibilities(Tree)
     */
    public static void display_tree_possibilities_score(Node n,int prof,int espace){
        if(n==null || prof<0) return ;
        
        //display_tray(n.getTray(),espace);
        for(int x=0;x<espace;x++) System.out.print("  ");
        System.out.println(n.getScore());
        if(n.getChildren()!=null){
            Maillon cour=n.getChildren().getFirst();
            while(cour!=null){
                /*char[][] grille=cour.getNoeud().getTray();
                display_tray(grille,espace);*/
                display_tree_possibilities_score(cour.getNoeud(), prof-1,espace+1);
                cour=cour.getNext_brother();
            } 
        }
    }
    
    /**
     * methode pour afficher le contenu de la grille avec un certains charadisign
     * @param plateau le plateau du jeu
     */
    private static void display_tray(char[][] plateau,int espace){
        for(int x=0;x<espace;x++) System.out.print("      ");
        System.out.println(/*"Plateau actuelle :\b"*/);
        for(int l=0;l<plateau.length;l++){
            
            if(l==0){//avant la cretaion de la grille affiche les colonnes jouales
                /*
                for(int x=0;x<espace;x++) System.out.print("  ");
                
                for(int l1=1;l1<=plateau[0].length;l1++){//calcul math
                    if(l1==1)System.out.print("  ");
                    if(l1<10) System.out.print("   "+l1);
                     else System.out.print("  "+l1);
               
                }
                System.out.println();*/
                //for(int x=0;x<espace;x++) System.out.print("      ");
                /*
                for(int x=1;x<=fils;x++) {
                    if(x==1)System.out.print(x);
                    else if(x==fils) System.out.println("-"+x);
                    else System.out.print("-"+x);
                }*/
                
                for(int x=0;x<espace;x++) System.out.print("      ");
                
                for(int l1=0;l1<=(plateau[0].length)*4;l1++){//des trait pour separer les numeros des colonnes jouables et la grille
                    if(l1==0)System.out.print("");
                    System.out.print("=");//fai
                }
                System.out.println();
            }
            
            for(int x=0;x<espace;x++) System.out.print("      ");
            
            for(int l2=0;l2<plateau[l].length;l2++){
                //affiche dabord le numero de la ligne  
                /*
                int line=plateau.length-l;
                if(line<10 && l2==0 ) System.out.print(line+"  ");
                 
                else if(l2==0)System.out.print(line+" ");*/
                //affiche les colonne correspondant � la ligne
                System.out.print("| "+plateau[l][l2]+" ");
            }
            
            System.out.println("|");//ferme la colonne et va � la ligne pour la construction suivantes
            
            for(int l1=0;l1<=(plateau[0].length)*4;l1++){//affiche des traits pour separer les lignes des colonnes 
                    
                    if(l1==0){
                        for(int x=0;x<espace;x++) System.out.print("      ");
                        
                        System.out.print("");
                    }
                    System.out.print("=");
                }
            System.out.println();
            
        }
    }
}
