package Partie2_HvsIA.HvsIA_naive.Arbre;

import Partie1_HvsH.*;
/**
 * D�crivez votre classe Node ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class Node
{
      
    private char[][] tray_n;//le tableau du noeud
    private char tour;
    private int score;
    private List children;
    
    /**
     * Constructeur d'objets de classe Node
     */
    public Node(char[][] tray,char ch)
    {
        // initialisation des variables d'instance
        tray_n=tray;
        //jeton_couleur();
        this.tour=ch;
        this.children=null;
        
    }
    
    public Node(char[][] tray)
    {
        // initialisation des variables d'instance
        tray_n=tray;
        this.children=null;
        
    }
    
    /**
     * methode qui renvoie le charactere ou siot le joueur qui doit jouer ce tour
     * @return renvoie un charactere
     */
    public void jeton_couleur(){
        int jeton_X=Tray.nb_token_color_tray(tray_n,'X');
        int jeton_O=Tray.nb_token_color_tray(tray_n,'O');
        
        if(Tray.grille_vide(tray_n)) tour= 'O';
        else if(jeton_X>jeton_O) tour= 'X';
        else tour= 'O';
    }
    
    /**
     * Methode geter qui renvoi le plateau du jeu en 2D
     * @return renvoie un tabelau de charactere � 2D
     */
    public char[][] getTray(){
        return this.tray_n;
    }
    
    /**
     * Methode getter qui renvoie le jeton de couleur
     * @return renvoie un charactere
     */
    public char getTour(){
        return this.tour;
    }
    
    /**
     * Methode getter qui renvoie le score du plateau actuelle (1 pour le joueur 'O', -1 pour l'adversare 'X' et 0 pour valeur absolue (personne � gagner))
     * @return renvoie un entiere qui est le score
     */
    public int getScore(){
        return this.score;
    }
    
    /**
     * Methode setter qui met � jouer ou modifie le score du plateau actuelle
     */
    public void setScore(int n){
        this.score=n;
    }
    
    /**
     * methode getter qui renvoie la List des enfants du noeud
     * @return renvoie une liste
     */
    public List getChildren(){
        return this.children;
    }
    
    /**
     * Methode setter 
     */
    public void setChildren(List l){
        this.children=l;
    }
    
    
    
}
