package Partie2_HvsIA.HvsIA_naive.HvsIA;

import Partie1_HvsH.*;
import Partie1_HvsH.ClasseException.*;
import java.util.Scanner;
import java.lang.Math;
import java.util.*;
/**
 * D�crivez votre classe HumainVsIA ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class HumainVsIA
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private Players player1;//le joueur 1
    private Players player2;//le joueur 2
    private Players player;//joueur quelconque, qui va servir pour joueur à tour de role
    private char[][] tray;//plateau du jeu 
    private ArrayList<Integer> posible_column_IA;//les colonnes o� l'IA peut jouer, tant qu'ils sont pas plein, (pour ne pas avoir de repetition)
    private int max_jeton;//donne le nombre de pions maximale pouvant etre jouer par un joueur 
    
    /**
     * Constructeur d'objets de classe HumainVsIA
     */
    public HumainVsIA(){
        System.out.println("\nATTENTION : Si vous Souhaitez faire une sauvegarde au cours de la partie tapez 's' ou 'S'");
        for(int x=0;x<=38;x++)System.out.print("=="); //separer cette precision par les autres phrases a venir au debut
        HumainVsIA_tout();
    }
    
    /**
     * methode regroupant les autres fonction et methode pour permettre une partie Humain VS IA
     */
    public void HumainVsIA_tout()
    {
        Scanner scanner=new Scanner(System.in);
        System.out.println("\nSi vous Souhaitez commencer une nouvelle parti taper 'n' ou 'N'");
        System.out.println("\nSi vous Souhaitez  recuperer votre parti precedent taper 'r' ou 'R'");
        String n =scanner.nextLine();
        switch(n){
            case "N":
                HumainVsIA_nouvaux();//commence une nouvelle partie 
                break;
          
            case "n":
                HumainVsIA_nouvaux();   
                break;    
                
            case "R":
                HumainVsIA_recuperer();//recupere une partie deja jouer
                break;
                
            case "r":
                HumainVsIA_recuperer();
                break;       
        
            default : 
                HumainVsIA_tout();  
        }
    }
    
    /**
     * Methode qui initialise une partie humain vs IA
     */
    public void HumainVsIA_nouvaux(){
        initier_joueurs_nouveaux();
        test();
        this.max_jeton=(this.tray.length*this.tray[0].length)/2;
        posible_column_IA=new ArrayList<>();
        //cree l'arrayliste des colonnes jouables par l'IA
        for(int x=0;x<tray.length;x++){
            posible_column_IA.add(x);
        }
        jouer(player1);//peut-etre faire un random qui va choisir un joueur au hasard pour debuter
    }
    
    /**
     * Methode qui initalise la grille
     */
    private void test(){
        try{
            this.tray=Tray.build_tray();

        }catch (ArithmeticException e){
            System.out.println(e.getMessage());
            test();
        }catch (IllegalArgumentException e){
            //System.out.println(e.getMessage());
            test();
        }
    } 
    
    /**
     * methode pour initialiser les deux joueurs qui vont s'affronter ( le deuxieme joueur est l'IA)
     */
    private void initier_joueurs_nouveaux(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("\nSaisissez le nom de votre joueur");
        this.player1=new Players(scanner.nextLine(), EnumCase.getEnumCase(EnumCase.X));
        this.player2=new Players("IA",EnumCase.getEnumCase(EnumCase.O));
        this.player=null;
    } 
    
    /**
     * Methode pour recuperer une partie jouer contre l'IA
     */
    public void HumainVsIA_recuperer()
    {
        //initialisation des variables d'instance
        Scanner scan=new Scanner(System.in);
        System.out.println("Nom de la personne qui a jouer la derniere fois");
        String n1=scan.nextLine();
        this.tray=Sauv_Recup_Partie.Recup_Partie("HvsIA",n1+"_"+"IA");
        this.max_jeton =(this.tray.length*this.tray[0].length)/2;

        initier_joueurs_recuperer();//initialise les joueurs
        posible_column_IA=new ArrayList<>();
        //cree l'arrayliste des colonnes jouables par l'IA
        for(int x=0;x<tray.length;x++){
            posible_column_IA.add(x);
        }
        sup_no_posible_column_IA();//supprimes le colonnes plein (car il s'agit d'une recuperation
        jouer(player);
    }  
    
    /**
     * methode pour initialioser les deux joueurs qui vont s'affronter apres une recuperation de la partie contre une IA
     */
    private void initier_joueurs_recuperer(){

        this.player1=Sauv_Recup_Partie.getP1();
        player1.setRecup_token(Tray.nb_token_color_tray(this.tray,EnumCase.getEnumCase(EnumCase.X)));
        this.player2=Sauv_Recup_Partie.getP2();
        player2.setRecup_token(Tray.nb_token_color_tray(this.tray,EnumCase.getEnumCase(EnumCase.O)));
        if(player1.getTotal_token()>player2.getTotal_token()) this.player=player2;
        else if(player1.getTotal_token()<player2.getTotal_token()) this.player=player1;
        else player=player1;
    }    
    
    /**
     * Methode qui supprime les colonnes qui ne sont jouable par l'IA (car colonne plein)
     */
    private void sup_no_posible_column_IA(){
        for(int x=0;x<tray.length;x++){
            if(tray[0][x]!=EnumCase.getEnumCase(EnumCase.VIDE) && posible_column_IA.contains(x) ){//verifie si la colonne est plein et si la liste contines cette colonne si oui le supprime 
                int ind=posible_column_IA.indexOf(x);//recupere l'indice de la colonne plein dans la liste et le supprime
                posible_column_IA.remove(ind);
            }
        }
    }
    
    /**
     * Methode qui copie posible_column_IA
     * @return renvoie une copie de posible_column_IA
     */
    private ArrayList<Integer> posible_column_IA_copie(){
        ArrayList<Integer> copie=new ArrayList<>();
        for(int x=0;x<posible_column_IA.size();x++){
            copie.add(posible_column_IA.get(x));
        }
        return copie;
    }
    
    /**
     * methode permettant a� deux joueur de s'affronter et de determiner qui gagne, ou mathc null
     * @param play le joueur qui va debuter la partie
     */
    private void jouer(Players play) {
        try{
            player=play;//le joueur actuelle qui joue
            while( !Players.egalite(player1,player2,max_jeton)){
                Tray.display_tray(this.tray);
                System.out.println("c'est le tour de "+player.getNom()+ "  || jeton : "+player.getColor_token());
                int column=0;
                if(player==player1) column=Tray.input_value(Tray.copie(tray),player1.copie(),player2.copie())-1;//column-1 car les colonnes sur les plateau sont compris entre [0,n-1]
                //on envoie une copie car on fait des test donc il ne faut pas modifier la veritable grille � ce niveau
                else column=Empecher_Gagner_IA.empecher_gagner(Tray.copie(tray),posible_column_IA_copie());//(int) (Math.random()*tray[0].length)+0;
                
                if(column==-2) {//si c'est -2 alors il s'agit d'une sauvegarde alors on arrete le jeu apres
                    System.out.println("La partie � bien �t� sauvegarder");
                    break;// le jeu s'arrete
                }
                
                if(column>tray[0].length-1 || column<0) throw new ColumnException("Veuillez saisir des colonne entre 1 et "+tray[0].length);
                player.setTotal_token();//augmente les pion de 1 a chaque fois que le joueur en question joue
                int line=Tray.play_column(tray,player.getColor_token() ,column);//recupere la derniere ligne ou le pion a ete poser par le joueur
                
                System.out.println();
                if(player==player2) System.out.println("L'IA � jouer dans la colonne " +(column+1));//affiche la colonne jou� par l'IA
                 
                if(Players.egalite(player1,player2,max_jeton) ) {//en cas d'égaliter, ou le nb de pion des deux joueur ont été utiliser
                    Tray.display_tray(this.tray);
                    System.out.println("~~~~~~~~~~~~~~~~~~Nous avons une egaliter, car tous les jeton ont �t� jouer~~~~~~~~~~~~~~~~~~~~~~~~");
                }
                
                if(Gagner.win_game(this.tray,line,column)) {//victoire pour l'un des joueur qui a reussi à faire un alignement de 4 jeton
                    Tray.display_tray(this.tray);
                    System.out.println(player.toString());
                    System.out.println();//pour revenir à la ligne
                    break;
                }
                
                sup_no_posible_column_IA();//supprime de la liste la colonne qui est pleine
                System.out.println();
                if(player.equals(player1)) player=player2;//si player1 à jouer alors ca sera au tour de player2
                else player=player1;

            }
            
        }catch (ColumnException  e){//si exception lever, alors qq1 a� jouer en dehors de l'intervalle  
                                    // des colonne autorise ou si la colonne est plein , donc il devra rejouer
            System.out.println(e.getMessage());
            if(player.equals(player1)) jouer(player1);
            else jouer(player2);
            
        }catch (ValueException i){ //seule chiffre est attendu sinon exception se leve
            
            System.out.println(i.getMessage());
            if(player.equals(player1)) jouer(player1);
            else jouer(player2);
            
        }
    }
}
