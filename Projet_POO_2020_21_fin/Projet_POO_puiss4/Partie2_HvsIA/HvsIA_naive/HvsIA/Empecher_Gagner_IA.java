package Partie2_HvsIA.HvsIA_naive.HvsIA;

import Partie1_HvsH.*;
import Partie1_HvsH.ClasseException.*;
import java.util.*;
/**
 * Cette classe va permettre a l IA d'eesayer en empechant son adversaire de gagner tout en se creant des opportunit� pour gagner.
 *
 * @author (KOUASSI et ASHENAFI)
 */
public class Empecher_Gagner_IA
{
    // variables d'instance - remplacez l'exemple qui suit par le votre
    /**
     * Constructeur d'objets de classe Empecher_Gagner_IA
     */
    public Empecher_Gagner_IA()
    {
        // initialisation des variables d'instance
        
    }

    /**
     * Fonction qui va retourner la colonne o� l'IA devra jouer apres plusieurs simulation en augmentant ses probabilites de gagner et diminuant celles de l'adversaire
     * @return renvoie la colonne o� l'IA jouera
     */
    public static int empecher_gagner(char[][] tray, ArrayList<Integer> possible){
        
        //le charactere X est jouer par l'adversaire et le O par l'IA
        //l'ordre de traitement est tres important
        try{
            //verifie si l'IA peut gagner ce tour
            for(int x=0;x<tray[0].length;x++){
                char[][] grille=Tray.copie(tray);//ne pas alterer le veritable grille; et car on fait plusieurs test avec une nouvelle grille � chaque fois
                int line=Tray.play_column(grille,EnumCase.getEnumCase(EnumCase.O) ,x);//l'IA fait une simulation en jouant son jeton
                if(Gagner.win_game(grille,line,x)) return x;//verifie s'il a gagner apres avoir jouer, s'il gagne alors il renvoie la colonne o� il a du jouer pour gagner
            }
        
            //verifie si l'adversaire � une possiblede gagner au prochain tour et le bloque si c'est le cas
            for(int x=0;x<tray[0].length;x++){
                char[][] grille=Tray.copie(tray);
                int line=Tray.play_column(grille,EnumCase.getEnumCase(EnumCase.X) ,x);//l'IA fait une simulation en jouant le jeton de l'adversaire
                if(Gagner.win_game(grille,line,x)) return x;//verifie si l'adversaire a gagner apres avoir jouer son jeton,
                                                                //s'il gagne alors il renvoie la colonne o� il a du jouer pour gagner, pour empecher l'adversaire de gagner
            }
        
            //L'IA fait une simulation pour voir si l'adversaire peut aligner 3 jeton ou 4 dans les deux prochaines tours et le bloque si c'est le cas
            //Uniquement les aligmenet horiezontale pour pouvoir aussi maximiser les chances de victoire de l'IA, pour ne pas perdre de temps � tout bloquer
            for(int x=0;x<tray[0].length-3;x++){
                char[][] grille=Tray.copie(tray);
                int line=Tray.play_column(grille,EnumCase.getEnumCase(EnumCase.X) ,x);
                for(int y=1;y<=3;y++){//on fait une autre simulation dans les 3 prochaine colonne � partir de la colonne de x
                    char[][] grille_aux=Tray.copie(grille);
                    int line_aux=Tray.play_column(grille_aux,EnumCase.getEnumCase(EnumCase.X) ,x+y);//on joue un autre jeton de l'adversaire et verifier si un alignement de 3 jeton ou plus sont present
                    if(Gagner.win_game(grille_aux,line_aux,x+y)) return x+y;//s'il gagne alors on l'empeche de faire les 3 alignement dont chaque extremite est separer par case vide
                                                                                 //on renvoyant la case ou on a pu poser le jeton pour qu'il gagne
                }
            }

            //verifie si l'adversaire peut gagner diagonalemet, apres que l'IA � poser son jeton, pour ne pas le faire gagner par erreur de l'IA
            for(int x=0;x<tray[0].length-3;x++){
                char[][] grille=Tray.copie(tray);
                int line=Tray.play_column(grille,EnumCase.getEnumCase(EnumCase.O) ,x); 
                for(int y=0;y<=3;y++){
                    char[][] grille_aux=Tray.copie(grille);
                    int line_aux=Tray.play_column(grille_aux,EnumCase.getEnumCase(EnumCase.X) ,x+y);//pareille que la verification precedente mais cette fois-ci de facon diagonales
                    if(Gagner.win_game(grille_aux,line_aux,x+y)) return x+y;
                }
            }
        
            //l'IA essaie d'aligner 3 jeton � partir de 2 jeton uniquement
            for(int x=0;x<tray[0].length-3;x++){
                char[][] grille=Tray.copie(tray);
                int line=Tray.play_column(grille,EnumCase.getEnumCase(EnumCase.O) ,x);
                for(int y=0;y<=3;y++){
                    char[][] grille_aux=Tray.copie(grille);
                    int line_aux=Tray.play_column(grille_aux,EnumCase.getEnumCase(EnumCase.O) ,x+y);//l'ia va essayer d'aligne 4 jeton consecutive en simulant 
                    if(Gagner.win_game(grille_aux,line_aux,x+y)) return x;//si l'ia gagne donc l'alignement des 4 jeton � partir de 2 jeton � pu avoir lieu
                                                                              //donc on renvoie la postion pour faire 3 alignement 
                }
            }
        }catch (ColumnException e){
            e.getMessage();
        }
        int col=(int) (Math.random()*possible.size())+0;//prends un nombre au hasard entre 0 et la taille de la list o� sont stocker les colonnes possibles d'etre jouer par l'IA
        return possible.get(col);
    }
   
}
