
package Partie2_HvsIA.HvsIA_naive.Arbres_possibilites;
import Partie1_HvsH.*;
import Partie1_HvsH.ClasseException.*;

/**
 * D�crivez votre classe Tree ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class Tree
{
    
    private Node root;
    private static int prof_max;
    //private char[][] grille;
    /**
     * Constructeur d'objets de classe Tree
     * @param grille grille de base du jeu
     * @param prof la profonedeur max de l'arbre
     */
    public Tree(char[][] grille,int prof)
    {
        // initialisation des variables d'instance
        this.root=new Node(grille);
        root.jeton_couleur();
        
        if(prof>0) root.setChildren( new List (List.cree_tree_possibilities_aux1(Tray.copie(grille),root.getTour(),0,prof ) ));
        this.prof_max=prof;
    }
    
    /**
     * Constructeur surcharger
     * @param l le nombre de ligne pour construire un tableau � 2D
     * @param c le nombre de colonne pour construire un tableau � 2D
     * @param prof la profondeur max de l'arbre 
     */
    public Tree(int l,int c,int prof)
    {
        // initialisation des variables d'instance
        char[][] grille=Tray.build_grille_vide(l,c);
        this.root=new Node(grille);
        root.jeton_couleur();
        if(prof>0) root.setChildren( new List (List.cree_tree_possibilities_aux1(Tray.copie(grille),root.getTour(),0,prof ) )); 
        this.prof_max=prof;
    }
    
    /**
     * Fonction qui affiche l'arbre des possibilite jusqu'� une profendeur donn� (ATTENTION pas l'hauteur)
     * @see display_tree_possibilities_aux(Node,int, int)
     */
    public static void display_tree_possibilities(Tree tree,int p){
        display_tree_possibilities_aux(tree.root,p,0);
        //display_tray(tree.root.getChildren().getFirst().getNoeud().getTray(),1);
        //display_tray(tree.root.getChildren().getFirst().getNext_brother().getNoeud().getTray(),1);
    }
    
    /**
     * Fonction auxiliaire pour afficher l'arbre des possibilite
     * @see display_tree_possibilities(Tree)
     */
    private static void display_tree_possibilities_aux(Node n,int prof,int espace){
        if(n==null || prof<0) return ;
        if(prof>prof_max) prof=prof_max;//le profondeur maximale autoriser
        display_tray(n.getTray(),espace);

        if(n.getChildren()!=null){
            Maillon cour=n.getChildren().getFirst();
            while(cour!=null){
                /*char[][] grille=cour.getNoeud().getTray();
                display_tray(grille,espace);*/
                display_tree_possibilities_aux(cour.getNoeud(), prof-1,espace+1);
                cour=cour.getNext_brother();
            } 
        }
    }

    
    /**
     * methode pour afficher le contenu de la grille avec un certains charadisign
     * @param plateau le plateau du jeu
     */
    private static void display_tray(char[][] plateau,int espace){
        for(int x=0;x<espace;x++) System.out.print("      ");
        System.out.println(/*"Plateau actuelle :\b"*/);
        for(int l=0;l<plateau.length;l++){
            
            if(l==0){//avant la cretaion de la grille affiche les colonnes jouales
                
                for(int x=0;x<espace;x++) System.out.print("      ");
                
                for(int l1=0;l1<=(plateau[0].length)*4;l1++){//des trait pour separer les numeros des colonnes jouables et la grille
                    if(l1==0)System.out.print("");
                    System.out.print("=");//fai
                }
                System.out.println();
            }
            
            for(int x=0;x<espace;x++) System.out.print("      ");
            
            for(int l2=0;l2<plateau[l].length;l2++){

                System.out.print("| "+plateau[l][l2]+" ");
            }
            
            System.out.println("|");//ferme la colonne et va � la ligne pour la construction suivantes
            
            for(int l1=0;l1<=(plateau[0].length)*4;l1++){//affiche des traits pour separer les lignes des colonnes 
                    
                    if(l1==0){
                        for(int x=0;x<espace;x++) System.out.print("      ");
                        
                        System.out.print("");
                    }
                    System.out.print("=");
                }
            System.out.println();
            
        }
    }

}