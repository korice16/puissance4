package Partie2_HvsIA.HvsIA_naive.Arbres_possibilites;


/**
 * D�crivez votre classe Maillon ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class Maillon
{
    // variables d'instance - remplacez l'exemple qui suit par le v�tre
    private Node noeud;
    private Maillon next_brother;

    public Maillon(Node noeud)
    {
        this.noeud = noeud;
        this.next_brother = null;
        //this.gagner=0;
    } 

    
    /**
     * methode getter qui renvoie la grille contenu dans ce Maillon
     * @return renvoie la grille
     */
    public Node getNoeud()
    {
        return this.noeud;
    }
     
    /**
     * Methode getter qui renvoie le frere suivant de ce Maillon
     * @return renvoie un Maillon (plus precisement le frere suivant de ce Maillon)
     */
    public Maillon getNext_brother()
    {
        return next_brother;
    }
   
    /**
     * Methode setter qui modifie le frere suivant de ce maillon
     */
    public void setNext_brother(Maillon suiv)
    {
        this.next_brother = suiv;
    }
   
}
