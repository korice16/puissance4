package Partie2_HvsIA.HvsIA_naive.Arbres_possibilites;

import Partie1_HvsH.*;
import Partie1_HvsH.ClasseException.*;

/**
 * D�crivez votre classe List ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class List
{
    // variables d'instance - remplacez l'exemple qui suit par le v�tre
    private Maillon first;
    private char[][] tray_l;
    private int column;
    private char jeton_couleur;
    /**
     * Constructeur d'objets de classe List
     */
    public List(char[][] tray,char jeton)
    {
        // initialisation des variables d'instance
        this.column=0;
        this.first=null;
        this.tray_l=tray;
        this.jeton_couleur=jeton;
        
    }
    
    /**
     * Constructeur d'objets de classe List surcharger
     */
    public List(Maillon f)
    {
        // initialisation des variables d'instance
        this.column=0;
        this.first=f;
        //this.tray_l=tray;
        //this.jeton_couleur=jeton;
        
    }
    

    /**
     * Methode auxiliaire pour cree un arbe n-aire 
     * @return renvoie un maillon
     */
    public static Maillon cree_tree_possibilities_aux1(char[][] tray,char ch,int c, int prof){
        while(c<tray[0].length && tray[0][c]!=' ' ){//passe jusqu'� la colonne vide ou fin de colonne
            c++;
        } 
        
        if(c>=tray[0].length ) return null;//si colonne pas accessible
        char[][] tray_l_copie=Tray.copie(tray);
        int line=0;
        try{
            line=Tray.play_column(tray_l_copie,ch,c);//joue dans la colonne
        }catch (ColumnException e){
        
        } 
        Node n1=new Node(tray_l_copie,ch);
        if(Gagner.win_game(tray_l_copie ,line,c)){
            if(n1.getTour()=='X')  n1.setScore(-1);
            else n1.setScore(1);
        }
        Maillon m=new Maillon(n1);
        if(!Tray.grille_plein(n1.getTray()) && !Gagner.win_game(tray_l_copie ,line,c) && prof>1){//s'il a pas gagner ou que la grille n'est pas pleine continue a cree des liste
            //List l=new List(n1.getTray(),n1.getTour());
        
            if(ch=='X') n1.setChildren( new List (cree_tree_possibilities_aux1(Tray.copie(tray_l_copie),'O',0 ,prof-1  )));
            else n1.setChildren( new List (cree_tree_possibilities_aux1(Tray.copie(tray_l_copie),'X',0 ,prof-1  )));
            
            
        }
        m.setNext_brother(cree_tree_possibilities_aux1(tray,ch,c+1,prof));
        return m;
    } 
    
    /**
     * Methode pour cree un arbre n-aire
     * @return renvoie une liste
     *//*
    public static List cree_tree_possibilities(Node n,int prof){
        if(Tray.grille_plein(n.getTray())) return null;
        List l=new List(n.getTray(),n.getTour());
        
        if(n.getTour()=='X') l.first=cree_tree_possibilities_aux(n.getTray(),'O',0,prof);
        else l.first=cree_tree_possibilities_aux(n.getTray(),'X',0,prof);
        return l;
    }*/
    
    /**
     * Methode auxiliaire pour cree un arbe n-aire 
     * @return renvoie un maillon
     *//*
    private static Maillon cree_tree_possibilities_aux(char[][] tray,char ch,int c,int prof){
        while(c<tray[0].length && tray[0][c]!=' ' ){//passe jusqu'� la colonne vide ou fin de colonne
            c++;
        } 
        
        if(c>=tray[0].length ) return null;//si colonne pas accessible
        char[][] tray_l_copie=Tray.copie(tray);
        int line=0;
        try{
            line=Tray.play_column(tray_l_copie,ch,c);//joue dans la colonne
        }catch (ColumnException e){
        
        } 
        Node n1=new Node(tray_l_copie,ch);
        if(Gagner.win_game(tray_l_copie ,line,c)){
            if(n1.getTour()=='X')  n1.setScore(-1);
            else n1.setScore(1);
        }
        Maillon m=new Maillon(n1);
        if(!Tray.grille_plein(n1.getTray()) && !Gagner.win_game(tray_l_copie ,line,c) && prof>=0){//s'il a pas gagner ou que la grille n'est pas pleine continue a cree des liste
            n1.setChildren(cree_tree_possibilities(n1,prof-1));
        }
        m.setNext_brother(cree_tree_possibilities_aux(tray,ch,c+1,prof));
        return m;
    } */
    
    /**
     * methode getter qui renvoie le premier maillon de la liste
     * @return renvoie un maillon
     */
    public Maillon getFirst(){
        return this.first;
    } 
}
