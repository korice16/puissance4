package Partie1_HvsH;

import java.util.Scanner;
import java.io.*; 
import java.io.File;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.FileNotFoundException; 
import java.util.NoSuchElementException;


/**
 * D�crivez votre classe Sauv_Recup_Partie ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class Sauv_Recup_Partie
{
    private static final String schemin_dossier_HvsH="C:/Users/public/Puissance4/HvsH"; 
    private static final String schemin_dossier_HvsIA="C:/Users/public/Puissance4/HvsIA"; 
    private static Players p1;
    private static Players p2;
    /**
     * Constructeur d'objets de classe Grille
     */
    
    public Sauv_Recup_Partie(){
    
    } 
   
    
    /**
     * Fonction pour cr�e les dossiers o� la partie va �tre sauvegarder(deux dossier vontre etre cree, de Humain vs Humain et Humain vs IA, dans le repertoire publique de l'utilisateur)
     */
    private static void Creation_dossier(){
        if(!new File(schemin_dossier_HvsH).exists()){//verifie si le dossier existe
            // //cree un dossier s'il n'existe pas
            new File(schemin_dossier_HvsH).mkdirs();
 
        }
        
        if(!new File(schemin_dossier_HvsIA).exists()){//verifie si le dossier existe
            // //cree un dossier s'il n'existe pas
            new File(schemin_dossier_HvsIA).mkdirs();
 
        }
    }
    
    /**
     * Fonction qui va executer la sauvegarde d'une partie
     * @param dossier le nom dossier du dossier o� la partie va �tre sauvegarder (HvsH: Humain contre humain,par defaut sa sera HvsIA:humain contre IA)
     * @param grille le plateau(grille) du jeu  
     * @param pl1 le joueur 1 de la partie qui va �tre sauvegarder 
     * @param pl2 le joueur 2 de la partie qui va �tre sauvegarder
     */
    public static void Sauv_Partie(String dossier,char[][] grille, Players pl1,Players pl2){
        try {
            Creation_dossier();//cree les dossiers de sauvegarde
            
            String nom_fichier=pl1.getNom() +"_"+pl2.getNom();//le nom du fichier sera le nom des deux joeu
            BufferedWriter writer = null;
            if(dossier.equals("HvsH")) writer=new BufferedWriter (new FileWriter(schemin_dossier_HvsH+"/" + nom_fichier+ ".txt"));
            else writer=new BufferedWriter (new FileWriter(schemin_dossier_HvsIA+"/" + nom_fichier+ ".txt"));
            String s= grille.length +" "+grille[0].length+" ";//taille de la grille
            writer.write(s);//sauvegarde de la taille de la grille
            for(int i=grille.length-1; i>-1; i--){
                for(int j=0; j<grille[i].length;j++){
                    if(grille[i][j]==EnumCase.getEnumCase(EnumCase.VIDE))  writer.write('*');//pour les case vide on ecirs le charactere * dans le fichier
                   else writer.write(grille[i][j]);    //si on a le charactere X ou O, on l'ecris tel quelle                
                }
                writer.write(" ");
            }
            writer.write(pl1.getNom());//nom du 1er joueur 
            writer.write(" ");
            writer.write(pl2.getNom());//nom du 2eme joueur 
            writer.close(); 
        }catch(IOException e){
            System.out.println("Erreur de sauvegarde ");//e.printStackTrace();
        }
    }
    
    /**
     * Fonction qui va recuperer une partie d�j� jouer
     * @param dossier le type de dossier (joueur contre joueur ou joueur contre IA)
     * @param nom_file le nom du fichier � recuperer (on peut parler d'identifiant)
     */
    public static char[][] Recup_Partie(String dossier,String nom_file){
        char[][] grille= null; 
        try{
            File file = null;
            if(dossier.equals("HvsH")) file=new File(schemin_dossier_HvsH+"/"+ nom_file + ".txt");
            else file=new File(schemin_dossier_HvsIA+"/"+ nom_file + ".txt");
            Scanner scan = new Scanner(file); 
            int l_taille = Integer.parseInt(scan.next());
            int c_taille = Integer.parseInt(scan.next());
            grille= new char[l_taille][c_taille]; 
            int cpt=0;
            while(scan.hasNext()){ 
                for(int i=l_taille-1; i>-1; i--){
                    cpt=0;
                    if (scan.hasNext()) {
                        String word =scan.next();
                        for(int j=0 ; j<c_taille; j++){
                            if(word.charAt(cpt)=='*') grille[i][j]=EnumCase.getEnumCase(EnumCase.VIDE);
                            else grille[i][j]=word.charAt(cpt);
                            cpt++;
                        }
                    }
                }
                //on recupere le nom des deux joueur, pour initialiser un nouveau joueur
                if (scan.hasNext()) p1=new Players(scan.next(), EnumCase.getEnumCase(EnumCase.X)); 
                if (scan.hasNext()) p2=new Players(scan.next(), EnumCase.getEnumCase(EnumCase.O));
            }
        }catch(FileNotFoundException f) {
            System.out.println("Erreur de chargement ");//f.printStackTrace();
        } 
        return grille;
    }
   
    /**
     * Fonction getter qui renvoie une copie du 1er joueur de la partie qui viens d'etre recuperer
     * @return renvoie un Players, soit le joueur 
     */
    public static Players getP1(){
        return p1.copie();
    }
    
    /**
     * Fonction getter qui renvoie une copie du 2eme joueur de la partie qui viens d'etre recuperer
     * @return renvoie un Players, soit le joueur
     */
    public static Players getP2(){
        return p2.copie();
    }
    
}
