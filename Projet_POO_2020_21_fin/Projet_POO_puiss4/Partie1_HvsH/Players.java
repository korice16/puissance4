package Partie1_HvsH;


/**
 * Le joueur qui va participêr à la partie du puissance 4.
 *
 * @author (author)
 * @version (29/11/2020)
 */
public class Players
{
    
    private String nom;
    private char color_token;
    private int total_token;

    /**
     * Constructeur d'objets de classe Players
     * 
     * @param nom le nom du joueur
     */
    public Players(String nom,char jeton)
    {
        // initialisation des variables d'instance
        this.nom=nom;
        this.total_token=0;
        this.color_token=jeton;
    }
     
    /**
     * Methode qui permete de faire une copie
     * @return renvoie la copie du joueur en question
     */
    public Players copie(){
        return new Players(new String(this.nom),this.color_token);
    }
   
    /**
     * methode getter pour le nombre total de jeton
     * 
     * @return renvoie le nombre de jeton jouer jusqu'à actuellement  par le joueur
     */
    public int getTotal_token(){//renvoie le nb de pion
        return this.total_token;
    }
    
    /**
     * methode pour le retourner le nom du joueur
     * @return renvoie le nom du joueur
     */
    public String getNom(){
        return this.nom;
    }
    
    /**
     * Methode setter qui augmente le nombre de jeton jouer par le jouer de 1 à chaque appelle
     */
    public void setTotal_token(){
        this.total_token++;
    }
    
    /**
     * methode getter qui renvoie la couleur ou la forme de la piece jouer par le joueur
     * @return renvoie la couleur du jeton
     */
    public char getColor_token(){
        return this.color_token;
    }
    
    /**
     *Methode setter qui permet de mettre � jour le score d'un joueur(uitliser principalement lors de la recuperation d une partie)
     */
    public void setRecup_token(int total){ 
        this.total_token=total;
    }
    
    /**
     * verifie si les deux joueurs ont atteint le nombre max de jeton a� jouer, dans ce cas egalite
     * 
     * @return renvoie vrai s'il y a egalite sinon faux
     */
    static public boolean egalite(Players p1,Players p2, int max_jeton){
        
        return ((p1.getTotal_token()==max_jeton)&&(p2.getTotal_token()==max_jeton));//vraie si egaliter
    } 
    
    /**
     * methode toString qui renvoie une chaine de caractere 
     * @return renvoie une chaine caractere felicitant le joueur gagnant
     */
    public String toString(){
        return "~~~~~~~~~~~~~~~~~~~~~~~~~~ ||-~-~-~-~-FELICITATION A '"+this.nom +"', QUI EST LE GAGNANT-~-~-~-~-|| ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"; 
        
    }
}
