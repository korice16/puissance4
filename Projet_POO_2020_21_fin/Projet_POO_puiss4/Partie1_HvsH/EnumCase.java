

package Partie1_HvsH;

/**
 * Classe enumeration des jeton pour jouer une partie de puissance 4
 *
 * @author (KOUASSI et ASHENAFI)
 */
public enum EnumCase
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    X('X'),O('O'),VIDE(' ');
    private char charact;

    /**
     * Constructeur d'objet
     */
    private EnumCase(char c){
        this.charact=c;
    }
    
    /**
     * Fonction qui renvoie le charactere
     * @return renvoie un charactere
     */
    public static char getEnumCase(EnumCase k){
        return k.charact;
    }
     
}
