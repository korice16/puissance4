package Partie1_HvsH;

/**
 * Cette classe va permettre de verifier dans une grille si un joueur � gagner soit s'il a aligner 4 jeton de la mm couleur ou plus consecutivement
 *
 * @author (Kouassi et ASHENAFI)
 */
public class Gagner
{
    /**
     * Constructeur d'objets de classe Gagner
     */
    public Gagner()
    {
        // initialisation des variables d'instance
       
    }
    
    /**
     * possibilite de gagner de facon diagonale vers le bas gauche
     * @param tray la grille du jeu
     * @param l la ligne de la derniere piece jouer 
     * @param c la colonne de la derniere piece jouer
     * 
     * @return renvoie Vrai si quatre piece consecutif de la meme couleur en diagonale sont align�e  sinon Faux
     */
    private static boolean diag_left(char[][] tray,int l,int c){//test si 4 piece consecutif en diagonal sont align�e
        //inf et supp commence � 1 car on les utilise dans le while pour faire les verification, donc ne pas oublie de l'enlever plus tard
        int inf=1;//pieces precedentes 
        int sup=1;//pieces suivantes
        if(tray[l][c]!=EnumCase.getEnumCase(EnumCase.VIDE)){//une piece doit etre dans les coordonnees envoyer
            //condition pour les bord et on verifie l'etat de la piece suivante
            while( (l-sup)>=0 && (c+sup)<tray[0].length && (tray[l][c]==tray[l-sup][c+sup]) && sup<=3) sup++;
            //condition pour les bord et on verifie l'etat de la piece suivante
            while( (l+inf)<tray.length && (c-inf)>=0 && (tray[l][c]==tray[l+inf][c-inf]) && inf<=3) inf++;

        }
        return ((sup-1)+(inf-1)+1)>=4; //+1, pour la piece de reference qui n'a pas �t� inclus dans les calculs precedent;(sup-1),(inf-1) car on commence par 1 non par 0
    }
   
    
    /**
     * possibilite de gagner de facon diagonale vers le bas droite
     * @param tray la grille du jeu
     * @param l la ligne de la derniere piece jouer 
     * @param c la colonne de la derniere piece jouer
     * 
     * @return renvoie Vrai si quatre piece consecutif de la meme couleur en diagonale sont align�e  sinon Faux
     */
    private static boolean diag_right(char[][] tray,int l,int c){//test si 4 piece consecutif en diagonal sont align�e
        int inf=1;
        int sup=1;
        if(tray[l][c]!=EnumCase.getEnumCase(EnumCase.VIDE)){
            
            while( (l-sup)>=0 && (c-sup)>=0 && (tray[l][c]==tray[l-sup][c-sup]) && sup<=3) sup++;//les bord de la grille � ne pas depasser
        
            while( (l+inf)<tray.length && (c+inf)<tray[0].length && (tray[l][c]==tray[l+inf][c+inf]) && inf<=3) inf++;//les bord de la grille � ne pas depasser

        }
        return ((sup-1)+(inf-1)+1)>=4;
    }
    
    /**
     * possibilite de gagner de facon verticale
     * @param tray la grille du jeu
     * @param l la ligne de la derniere piece jouer 
     * @param c la colonne de la derniere piece jouer
     * 
     * @return renvoie Vrai si quatre piece consecutif de la meme couleur en verticale sont align�e  sinon Faux
     */
    private static boolean check_verti(char[][] tray,int l,int c){//test si 4 piece consecutif en vertical sont align�e
        int x=1;
        if(tray[l][c]!=EnumCase.getEnumCase(EnumCase.VIDE)){
            for(int y=1;y<4;y++){
                if((l+y)<tray.length){//Pour ne pas chercher en dehors du tableau
                    if((tray[l][c]==tray[l+y][c])) x++;//verifie si la couleur de la piece actuelle avec les pieces suivantes
                }
            }
        }
        return x==4;
    }
    
    /**
     * possibilite de gagner de facon horizontale
     * @param tray la grille du jeu
     * @param l la ligne de la derniere piece jouer 
     * @param c la colonne de la derniere piece jouer
     * 
     * @return renvoie Vrai si quatre piece consecutif de la meme couleur en horizontal sont align�e  sinon Faux
     */
    private static boolean check_hori(char[][] tray,int l,int c){//test si 4 piece consecutif et horizontal sont align�e
        int inf=1;
        int sup=1;
        if(tray[l][c]!=EnumCase.getEnumCase(EnumCase.VIDE)){
            
            while( (c+sup)<tray[0].length && (tray[l][c]==tray[l][c+sup]) && sup<=3) sup++;//les bord de la grille � ne pas depasser
        
            while((c-inf)>=0 && (tray[l][c]==tray[l][c-inf]) && inf<=3) inf++;//les bord de la grille � ne pas depasser

        }
        return ((sup-1)+(inf-1)+1)>=4;
    }
    
    /**
     * Cette classe reunis tous les autres classe et verifie si l'une est vraie
     * @param tray la grille du jeu
     * @param l la ligne de la derniere piece jouer 
     * @param c la colonne de la derniere piece jouer
     * 
     * @return renvoie Vrai si la partie a ete gagnee sinon Faux
     */
    public static boolean win_game ( char[][] tray,int l,int c){
        return (diag_left(tray, l, c) | diag_right(tray, l, c) | check_verti(tray, l, c) | check_hori(tray, l, c)) ? true:false;
    }
    
}
