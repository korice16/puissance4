

package Partie1_HvsH;
import java.util.Scanner;
import Partie1_HvsH.ClasseException.*;

/**
 * Classe agissans directement sur le tableau,EX: constructionde tableau, permettre au joueur de jouer dans une colonne etc.
 *
 * @author (KOUASSI et ASHENAFI)
 */
public class Tray
{
    /**
     * Constructeur d'objets de classe Grille
     */
    public Tray()
    {
        
    }
    
    /**
     * Fonction qui construit un tableau de charactere � deux dimension
     * @renvoie un tableau de charactere � deux dimension
     */
    public static char[][] build_grille_vide(int l,int c)
    {
        
        char[][]grille= new char[l][c];
        for(int i=0; i<grille.length; i++){
            for(int j=0; j<grille[i].length;j++){
                grille[i][j]=EnumCase.getEnumCase(EnumCase.VIDE);
            }
        }
        return grille;
    }
    
    /**
     * fonction qui verifie si la grille est vide 
     * @return renvoie vrai si la grille est plein sinon faux
     */
    public static boolean grille_vide(char[][] grille){
         
        for(int j=0; j<grille[0].length;j++){
            if( grille[grille.length-1][j] != EnumCase.getEnumCase(EnumCase.VIDE))
            return false;
        }  
        return true;
    } 
    
    /**
     * Fonction qui verifie si une grille est plein
     * @return renvoie vrai si la grille est plein sinon faux
     */
    public static boolean grille_plein(char[][] grille){
       
        for(int j=0; j<grille[0].length;j++){//il suffit de regarder principalement les sommets de la colonne
            if(grille[0][j]== EnumCase.getEnumCase(EnumCase.VIDE)) return false ;
              
        }
        return true;
    }
    
    
    /**
     * Fonction qui permet de faire la copie d'un tabelau a deux dimension de charactere
     * @return renvoie la copie du tableau a deux dimension
     */
    public static char[][] copie(char[][] tray){
        char[][] grille=new char[tray.length][tray[0].length];
        
        for(int x=0;x<tray.length;x++){
            for(int y=0;y<tray[x].length;y++){
                grille[x][y]=tray[x][y];
        
            }
        
        }
        return grille;
    }
    
    /**
     * methode principal pour la construction de la grille
     * @see build_tray_aux()
     * @return renvoie la grille choisit par l'utilisateur
     */
    public static char[][] build_tray(){
        return build_tray_aux();
    }
    
    /**
     * Methode auxiliaire qui va permettre la construiction de la grille, par defaut ou par le client
     * @see build_tray()
     * @see construire_tray_aux()
     * @return renvoie la grille construite par l'utlisateur
     */
    private static char[][] build_tray_aux(){
        Scanner scan=new Scanner(System.in);
        char[][] grille=null;
        System.out.println("\nSouhaitez-vous construire votre grille ou choisir la grille par defaut '7x6?'");
        System.out.println("Ecrivez 'YES' ou 'yes' pour construire une grille ou 'NO'  ou 'no'pour choisir la grille par defaut");
        String choix=scan.nextLine();
        switch(choix){
            case "YES"://l'utilisateur consruire sa grille
                grille=build_tray_aux_aux();
                break;
                
            case "yes"://l'utilisateur consruire sa grille
                grille=build_tray_aux_aux();
                break;
            
            case "NO"://grille par defaut
                grille=build_grille_vide(6,7);
                break;
                
            case "no"://grille par defaut
                grille=build_grille_vide(6,7);
                break;
            
            default :
                System.out.println("Aucune reponse ne correspond aux choix proposer, veuillez reeseyer");
                throw new IllegalArgumentException();//si l'utilisateur n'a entree aucune de ces proposition, il devra repeter l'operation du choix de la grille
               
        }
        return grille;
    }
    
    
    /**
     * methode auxiliare pour la construction de la grille, si le client à choisit d'en costruire
     * @see construire_tray()
     * @see input_number
     * @return renvoie la grille construite par l'utilisateur
     */
    private static char[][] build_tray_aux_aux(){
        Scanner scan=new Scanner(System.in);
        char[][] grille=null;
        try{
            System.out.println("\nSi le nombre de ligne ou de colonne sont <4, vous allez devoir refaire le choix");
            System.out.println("Saisir nombre de colonne de la grille");
            
            int colon=input_value(null,null,null);//leve exception si ce n'est pas un chiffre
            if(colon<4) throw new ArithmeticException("Le nombre de colonne doit etre >= 4,veuillez reesayer");
            System.out.println("Saisir nombre de ligne de la grille");
            int ligne=input_value(null,null,null);
            if(ligne<4) throw new ArithmeticException("Le nombre de ligne doit etre >= 4, veuillez reesayer");
        
            if(colon>=4 && ligne>=4){
                //this.tray=Tray.grille(ligne,colon);
                grille=build_grille_vide(ligne,colon);
            }//refait la construction de la grille si ligne ou colonne <4
            
        }catch (ValueException e){
            //scan.next();
            throw new IllegalArgumentException();//recommence uniquement la saisit de la ligne et colonne, soit cette methode
        }
        return grille;
    }
    
    /**
     * methode qui permet de saisir un nombre entier et de le verifier, si la verification est fausse une exception est lever
     * @return renvoie un entier,soit la colonne ou le joueur � voulu jouer 
     */
    public static int input_value (char[][] tray, Players p1, Players p2) throws ValueException{
        Scanner scan=new Scanner(System.in);
        boolean test=scan.hasNextInt();
        boolean test1=scan.hasNextLine();
        if(!test) {//verifie s'il s'agit d'un nombre
            if (test1){//verifie s'il sagit d'une chaine de charactere
                String sauv=scan.nextLine();
                if(sauv.equals("s") || sauv.equals("S") ) {
                    if(!p2.getNom().equals("IA")) Sauv_Recup_Partie.Sauv_Partie("HvsH", tray , p1,p2);
                    else Sauv_Recup_Partie.Sauv_Partie("HvsIA", tray , p1,p2);
                    return -1;
                }else throw new ValueException();  
            }else throw new ValueException();
        }   
        int value=scan.nextInt();
        if(value<1) return -2; //pour pas connfondre avce la condition de sauvegarde qui renvoie -1; et dans tous les cas l'utilisateur n'est cense rentre que des nb >0
        return value;
    }
    
    /**
     * Permet de joueur dans une colonne et de renvoyer ma ligne ou le jton a ete poser
     * @param tray tableau a 2D ou l'on posera le jeton
     * @param jeton couleur ou forme du jeton a� jouer
     * @param column la colonne ou le jeton sera poser
     * @return renvoie la ligne ou la piece a ete poser
     */
    
    public static int play_column(char[][] tray,char jeton, int column)
    throws ColumnException{
        int line=0;//tray.length-1;//valeur temporaire pour stocker la ligne de la derniere piece jouer
        
        if(tray[0][column]==EnumCase.getEnumCase(EnumCase.VIDE) ){//si la collonne est vide
            if(tray[tray.length-1][column]==EnumCase.getEnumCase(EnumCase.VIDE)) { //verifie s'il y un jeton au fond de la grille, dans le cas contraire il le met
                tray[tray.length-1][column]=jeton;
                line=tray.length-1;
            }else{ 
                while(tray[line][column]==EnumCase.getEnumCase(EnumCase.VIDE) && line<tray.length-1){ //le cas s'il y a deja un jeton au fond 
                    if(tray[line+1][column]!=EnumCase.getEnumCase(EnumCase.VIDE)) tray[line][column]=jeton;
                    else line++;
                }
            }
        }else throw new ColumnException("\nLa colonne dans laquel vous avez jouer est plein, reesayez");
        
        return line;
    }
    
    /**
     * methode pour afficher le contenu de la grille avec un certains charadisign
     * @param plateau le plateau du jeu
     */
    public static void display_tray(char[][] plateau){
        System.out.println("\nPlateau actuelle :\b");
        for(int l=0;l<plateau.length;l++){
             
            if(l==0){//avant la cretaion de la grille affiche les colonnes jouales
                for(int l1=1;l1<=plateau[0].length;l1++){//calcul math
                    if(l1==1)System.out.print("  ");
                    if(l1<10) System.out.print("   "+l1);
                     else System.out.print("  "+l1);
               
                }
                System.out.println();
                for(int l1=0;l1<=(plateau[0].length)*4;l1++){//des trait pour separer les numeros des colonnes jouables et la grille
                    if(l1==0)System.out.print("   ");
                    System.out.print("=");
                }
                System.out.println();
            }
            
            for(int l2=0;l2<plateau[l].length;l2++){
                //affiche dabord le numero de la ligne  
                int line=plateau.length-l;
                if(line<10 && l2==0 ) System.out.print(line+"  "); 
                else if(l2==0)System.out.print(line+" ");
                //affiche les colonne correspondant � la ligne
                System.out.print("| "+plateau[l][l2]+" ");
            }
            
            System.out.println("|");//ferme la colonne et va � la ligne pour la construction suivantes
            for(int l1=0;l1<=(plateau[0].length)*4;l1++){//affiche des traits pour separer les lignes des colonnes 
                    if(l1==0)System.out.print("   ");
                    System.out.print("=");
                }
            System.out.println();
            
        }
    }
    
    /**
     * Fonction qui retourne le nombre total du jeton en question qui a ete jouer
     * @return renvoie un entier, le nobre total de jeton
     */
    public static int nb_token_color_tray(char[][] tray, char c){
        int pion=0;
        for(int i=0; i<tray.length; i++){
            for(int j=0; j<tray[i].length;j++){
                if(tray[i][j]==c)
                pion++;
            }
        }
        return pion;
    }
}
