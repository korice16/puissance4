
package Partie1_HvsH.ClasseException;

/**
 * Classe de type Exception, qui se leve quand on saisit autre chose qu'un chiffre
 *
 * @author (KOUASSI et ASHENAFI)
 *
 */
public class ValueException extends Exception{
    public ValueException(){
        System.out.println("\nUn chiffre uniquement est attendu ou 's' pour sauvegarder,reesayez");
    }
}