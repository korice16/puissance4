
package Partie1_HvsH.ClasseException;

/**
* Exception se levant lorsue l'on joue dans une colonne deja pleine
 *
 * @author (Kouassi et ASHENAFI)
 */
public class ColumnException extends Exception{
    public ColumnException(String son){
        super(son);
    }
}
