package Partie1_HvsH; 

import Partie1_HvsH.ClasseException.*;
import java.util.Scanner;
import java.lang.IllegalArgumentException;
import java.lang.Math;
import java.util.*;
import java.lang.NullPointerException;

/**
 * Classe reunissant touttes les classes necessaire a une partie de puissance4
 *
 * @author (KOUASSI et ASHENAFI)
 */

public class HumainVsHuman
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private Players player1;//le joueur 1
    private Players player2;//le joueur 2
    private Players player;//joueur quelconque, qui va servir pour joueur à tour de role
    private char[][] tray;//plateau du jeu 
    private int max_jeton;//donne le nombre de pions maximale pouvant etre jouer par un joueur
    
    /**
     * Constructeur d'objets de classe HumainVsHuman
     */
    public HumainVsHuman(){
        System.out.println("\nATTENTION : Si vous Souhaitez faire une sauvegarde au cours de la partie tapez 's' ou 'S'");
        for(int x=0;x<=43;x++)System.out.print("=="); //separer cette precision par les autres phrases a venir au debut
        HumainVsHuman_tout();
    }
    
    private void HumainVsHuman_tout(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("\nSi vous Souhaitez commencer une nouvelle parti taper 'n' ou 'N'");
        System.out.println("\nSi vous Souhaitez  recuperer votre parti precedent taper 'r' ou 'R'");
        String n =scanner.nextLine();
        switch(n){
            case "N":
                HumainVsHuman_nouvaux();//commence une nouvelle partie   
                break;
          
            case "n":
                HumainVsHuman_nouvaux();//commence une nouvelle partie   
                break;    
                
            case "R":
                HumainVsHuman_recuperer();//recupere une partie deja jouer
                break;
                
            case "r":
                HumainVsHuman_recuperer();//recupere une partie deja jouer
                break;    
        
            default : 
                HumainVsHuman_tout();//refait le choix  
        }
    }
    
    
    /**
     * Methode qui initialies une partie 
     */
    private void HumainVsHuman_nouvaux(){
        initier_joueurs_nouveaux();
        test();
        this.max_jeton=(this.tray.length*this.tray[0].length)/2;
        jouer(player1);//peut-etre faire un random qui va choisir un joueur au hasard pour debuter
     }
    
    /**
     * Methode qui initalise la grille
     */
    private void test(){
        try{
            this.tray=Tray.build_tray();

        }catch (ArithmeticException e){
            System.out.println(e.getMessage());
            test();
        }catch (IllegalArgumentException e){
            //System.out.println(e.getMessage());
            test();
        }
    } 
     
    /**
     * methode pour initialioser les deux joueurs qui vont s'affronter
     */
    private void initier_joueurs_nouveaux(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("\nSaisir nom du premier joueur");
        this.player1=new Players(scanner.nextLine(), EnumCase.getEnumCase(EnumCase.X));
        System.out.println("Saisir nom du deuxieme joueur");
        this.player2=new Players(scanner.nextLine(),EnumCase.getEnumCase(EnumCase.O));
        this.player=null;
    } 
    
    /**
     * Methode qui permettre la recuperation d'une partie deja joeur
     */
    public void HumainVsHuman_recuperer(){
        
        Scanner scan=new Scanner(System.in);
        System.out.println("Nom de la 1er personne");
        String n1=scan.nextLine();
        System.out.println("Nom de la 2eme personne");
        String n2 =scan.nextLine();
        this.tray=Sauv_Recup_Partie.Recup_Partie("HvsH",n1+"_"+n2);
        this.max_jeton =(this.tray.length*this.tray[0].length)/2;
        initier_joueurs_recuperer();
        
        jouer(player);//peut-etre faire un random qui va choisir un joueur au hasard pour debuter
    }  
    
    /**
     * methode pour initialioser les deux joueurs qui vont s'affronter
     */
    private void initier_joueurs_recuperer(){

        this.player1=Sauv_Recup_Partie.getP1();
        player1.setRecup_token(Tray.nb_token_color_tray(this.tray,EnumCase.getEnumCase(EnumCase.X)));
        this.player2=Sauv_Recup_Partie.getP2();
        player2.setRecup_token(Tray.nb_token_color_tray(this.tray,EnumCase.getEnumCase(EnumCase.O)));
        if(player1.getTotal_token()>player2.getTotal_token()) this.player=player2;
        else if(player1.getTotal_token()<player2.getTotal_token()) this.player=player1; 
        else player=player1;
    }
    
    /**
     * methode permettant a� deux joueur de s'affronter et de determiner qui gagne, ou mathc null
     * @param play le joueur qui va debuter la partie
     */
    private void jouer(Players play) {
        try{
            player=play;//le joueur actuelle qui joue
            while( !Players.egalite(player1,player2,max_jeton)){
                Tray.display_tray(this.tray);
                System.out.println("c'est le tour de "+player.getNom()+ "  || jeton :"+player.getColor_token());
                int column=Tray.input_value(Tray.copie(tray),player1.copie(),player2.copie())-1;//column-1 car les colonnes sur les plateau sont compris entre [0,n-1]
                if(column==-2) {//si c'est -2 alors il s'agit d'une sauvegarde alors on arrete le jeu apres
                    System.out.println("La partie � bien �t� sauvegarder");
                    break;// le jeu s'arrete
                }
                
                if(column>tray[0].length-1 || column<0) throw new ColumnException("Veuillez saisir des colonne entre 1 et "+tray[0].length);
                player.setTotal_token();//augmente les pion de 1 a chaque fois que le joueur en question joue
                int line=Tray.play_column(tray,player.getColor_token() ,column);//recupere la derniere ligne ou le pion a ete poser par le joueur
                
                if( Players.egalite(player1,player2,max_jeton) ) {//en cas d'egaliter, ou le nb de pion total des deux joueur ont ete utiliser
                    Tray.display_tray(this.tray);
                    System.out.println("~~~~~~~~~~~~~~~~~~Nous avons une egaliter, car tous les jeton ont �t� jouer~~~~~~~~~~~~~~~~~~~~~~~~");
                }
                
                if(Gagner.win_game(this.tray,line,column)) {//victoire pour l'un des joueur qui a reussi a� faire un alignement de 4 jeton
                    Tray.display_tray(this.tray);
                    System.out.println(player.toString());
                    System.out.println();//pour revenir à la ligne
                    break;
                }
                
                System.out.println();
                if(player.equals(player1)) player=player2;//si player1 a� jouer alors ca sera au tour de player2
                else player=player1;

            }
            
        }catch (ColumnException  e){//si exception lever, alors qq1 a jouer en dehors de l'intervalle  
                                    // des colonne autorise ou si la colonne est plein , donc il devra rejouer
            System.out.println(e.getMessage());
            if(player.equals(player1)) jouer(player1);
            else jouer(player2);
            
        }catch (ValueException i){ //seule chiffre est attendu sinon exception se leve
            
            System.out.println(i.getMessage());
            if(player.equals(player1)) jouer(player1);
            else jouer(player2);
            
        }
    }
    
}



